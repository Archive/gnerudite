/* Gnerudite.c
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "Gnerudite.h"
#include "main.h"
/* TODO Theme the rack! */

int
main (int argc, char *argv[])
{

  /* Initialise i18n */
  bindtextdomain (PACKAGE, GNOMELOCALEDIR);
  textdomain (PACKAGE);

  /* ------------------------------------------------------- */
  /* Initialize GNOME, this is very similar to gtk_init */
  gnome_init ("Gnerudite", VERSION, argc, argv);

  /* Initialise random numbers */
  srand (time (0));

  /* Session Management */
  init_session_management (argv[0]);

  /* Create Instance of The App */
  window = gnome_app_new ("Gnerudite", "Gnerudite");

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      (GtkSignalFunc) destroy, NULL);
  gtk_window_set_policy (GTK_WINDOW (window), TRUE, TRUE, FALSE);
  gtk_container_set_border_width (GTK_CONTAINER (window), 0);

  /* Load User Preferences */
  setup_preferences ();

  /* Create Menu items */
  if (debug)
    printf ("About to set the menu-bar and status bar\n");
  status = gnome_appbar_new (FALSE, TRUE, GNOME_PREFERENCES_NEVER);
  gnome_app_set_statusbar (GNOME_APP (window), status);
  install_menus_and_toolbar (window);
  if (debug)
    printf ("I have set the menu-bar and status bar\n");

  /* This creates stuff to hold scores, buttons etc. */
  init_create_score_and_buttons ();

  /* Create a new scrolled window. */
  main_window_vbox = gtk_vbox_new (FALSE, 0);
  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  gtk_container_set_border_width (GTK_CONTAINER (scrolled_window), 0);
  /* the policy is one of GTK_POLICY AUTOMATIC, or GTK_POLICY_ALWAYS.
   * GTK_POLICY_AUTOMATIC will automatically decide whether you need
   * scrollbars, whereas GTK_POLICY_ALWAYS will always leave the scrollbars
   * there.  The first one is the horizontal scrollbar, the second,
   * the vertical. */
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW
				  (scrolled_window),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start (GTK_BOX (main_window_vbox),
		      scrolled_window, TRUE, TRUE, 0);
  gtk_widget_show (scrolled_window);
  TOMTRACE (("Created a scrolled window\n"));

  /* Initialise scores */
  initialise_tile_scores ();

  /* A 15 by 15 array of tile_buttons to simulate the board. */
  init_create_board ();

  /* Create the tile-racks to hold the players' tiles */
  init_create_rack ("Player one");
/*  init_create_rack ("Player Two");  */

  /* Create the letter bag */
  letter_bag_create ();

  gtk_box_pack_start (GTK_BOX (main_window_hbox),
		      main_window_vbox, TRUE, TRUE, 0);
  gtk_widget_show (main_window_vbox);

  /* This Positions the window in the top-left and sizes it */
  gtk_widget_set_uposition (window, 0, 0);
/*  gtk_widget_set_usize (window, 592, 597); */
  gtk_widget_set_usize (window, 697, 709);

  /* This is to show the window */
  gtk_widget_show (window);

  /* All GTK applications must have a gtk_main(). Control ends here
   * and waits for an event to occur (like a key press or
   * mouse event). */
  gtk_main ();
  return (0);
}

void
destroy (GtkWidget * widget, gpointer data)
{
  save_user_preferences ();
  gtk_main_quit ();
}

void
set_tooltip (GtkWidget * w, const gchar * tip)
{
  GtkTooltips *t = gtk_tooltips_new ();

  gtk_tooltips_set_tip (t, w, tip, NULL);
}
