/* Gnerudite.h
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*** Gnerudite_h */
#ifndef GNERUDITE_H
#define GNERUDITE_H

#include "config.h"
#include <gnome.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <gtk/gtk.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>

#define DICTSIZE 119000

#define TOMTRACE(a) { \
    if(debug) \
    { \
    printf("%s +%u : ",__FILE__,__LINE__); \
            printf a; \
	    fflush(stdout); \
    } \
}

/* For tracking of special tile scores */
typedef enum
{ SCORE_NONE,
  SCORE_DOUBLE_LETTER,
  SCORE_TRIPLE_LETTER,
  SCORE_DOUBLE_WORD,
  SCORE_TRIPLE_WORD
}
tile_score;

/* Used to pass error messages back if a turn is invalid */
typedef enum
{ NO_ERROR,
  TURN_INVALID_NOT_IN_SAME_ROW,
  TURN_INVALID_NOT_IN_SAME_COL,
  TURN_INVALID_DIAGONAL_WORD,
  TURN_INVALID_NO_WORD_MATCH,
  TURN_INVALID_GAP_IN_WORD,
  TURN_INVALID_GO_CANCELLED,
  TURN_INVALID_NOT_ADJACENT,
  TURN_INVALID_NO_SECONDARY_WORD_MATCH,
  TURN_INVALID_ONLY_ONE_TILE,
  TURN_SKIPPED
}
turn_valid_result;

typedef enum
{ TILE_SWAP_RELEASE,
  TILE_SWAP_PRESS,
  TILE_SWAP_DRAG
}
tile_swap_type;

/* Just the direction of the word placed this go */
typedef enum
{ WORD_UNKNOWN,
  WORD_RIGHT,
  WORD_DOWN
}
direction;

/* A grid of 15x15 of these form the scrabble board. */
/* TODO bitfields */
typedef struct
{
  GtkWidget *toggle_button;
  GtkWidget *pixmapwid;
  GtkWidget *label;
  GtkWidget *hbox;
  GtkWidget *score_label;
  GtkWidget *tile_table;
  gchar letter;
  int placed_this_go;
  int row;
  int col;
  tile_score score;
  int is_wildcard;
}
grid_button;

/* An array of 13 of these make up the player's rack */
typedef struct
{
  GtkWidget *toggle_button;
  GtkWidget *pixmapwid;
  GtkWidget *label;
  GtkWidget *score_label;
  GtkWidget *tile_table;
  gchar letter;
  int col;
  int is_wildcard;
}
rack_button;

typedef struct
{
  char *tile_font;
  char *tile_theme_dir;
  char *dictionary_file;
  char *permutations_file;
  int confirm_skip_go;
  tile_swap_type tile_swap_mode;
  int magic;
  int show_key;
  int cpu_max_word_length;
  int cpu_max_score;

  /* The property window widgets */
  GtkWidget *propwindow;
  GtkWidget *theme_entry;
  GtkWidget *host_entry;
  GtkWidget *font_entry;
  GtkWidget *dictionary_entry;
}
user_preferences;

/* ****** FUNCTIONS ******** */
void destroy (GtkWidget * widget, gpointer data);
char pick_letter_from_bag (void);
void grid_square_toggled (grid_button *);
void reset_grid_buttons (void);
int tiles_can_be_swapped (grid_button *, grid_button *);
void reset_rack_buttons (void);
int rack_tiles_can_be_swapped (rack_button *, rack_button *);
int place_rack_tile (rack_button *, grid_button *);
void rack_tile_toggled (rack_button *);
int rack_tile_can_be_placed (rack_button *, grid_button *);
int swap_rack_tiles (rack_button *, rack_button *);
int swap_grid_tiles (grid_button *, grid_button *);
void deal_seven_letters (void);
int grid_show_label_hide_pixmap (grid_button *);
int grid_show_pixmap_hide_label (grid_button *);
int go_ended (void);
int go_is_valid (void);
void cb_deal_button (void);
int clear_grid_button_validations (void);
int cb_skip_go_pressed (void);
int count_placed_tiles_in_row (int);
int count_placed_tiles_in_col (int);
int reset_last_go (void);
int load_user_preferences (void);
int save_user_preferences (void);

void show_preferences_dialog (GtkWidget * widget, gpointer data);
int show_about_dialog (void);
void menu_item_functions (gchar *);
void populate_theme_list (GtkWidget * clist);
gint sort_theme_list_cb (void *a, void *b);
void die (GnomeClient * client, gpointer client_data);
int save_yourself (GnomeClient * client, int phase,
		   GnomeSaveStyle save_style, int shutdown,
		   GnomeInteractStyle interact_style, int fast,
		   gpointer client_data);
void skip_go_cb (GtkWidget * w, gpointer data);
void tile_swap_release_cb (GtkWidget * w, gpointer data);
void tile_swap_press_cb (GtkWidget * w, gpointer data);
void tile_swap_drag_cb (GtkWidget * w, gpointer data);
void change_theme (void);
void change_tile_swap_mode (void);
void magic_cb (GtkWidget * w, gpointer data);
void more_magic_cb (GtkWidget * w, gpointer data);
void font_button_pressed (GtkWidget * w, gpointer data);
void return_tile_to_rack (grid_button *);
void set_tile_status (grid_button *);
void reset_tile_status (grid_button *);
void load_dictionary (void);
void install_menus_and_toolbar (GtkWidget * app);
int start_next_go (void);
int wordsearch (char *);
void theme_selected_cb (GtkWidget * clist, gint row, gint column,
			GdkEventButton * bevent, gpointer data);
void change_tile_font (void);
void font_sel_ok (GtkWidget * w, GtkWidget * fsel);
void font_sel_cancel (GtkWidget * w, GtkWidget * fsel);
void font_button_pressed (GtkWidget * w, gpointer data);
void cb_jumble_button (void);
void tidy_rack (void);
void setup_preferences (void);
void initialise_tile_scores (void);
void init_create_board (void);
void init_create_rack (char *player_name);
void init_create_score_and_buttons (void);
void init_session_management (char *name);

void letter_bag_push (gchar c);
void letter_bag_mix (void);
void letter_bag_create (void);
gchar letter_bag_pop (void);
void rack_button_assign_char (rack_button * rackbutton, gchar c);
void rack_button_assign_score (rack_button * rackbutton, gchar c);
void grid_button_assign_char (grid_button * gridbutton, gchar c);
void grid_button_assign_score (grid_button * gridbutton, gchar c);
int are_tiles_adjacent (grid_button * placed_tiles[],
			int tiles_placed_this_go);
void cb_swap_tile_button (void);
void cb_swap_dialog_clicked (GtkWidget * dialog, int button, gpointer * data);
int cb_swap_dialog_closed (GtkWidget * dialog);
gchar ask_user_for_wildcard_char (int row, int col);
void set_tooltip (GtkWidget * w, const gchar * tip);
int calc_score_for_tile (grid_button * gridbutton, short *, short *);
int get_final_word_score (int wordscore, int double_word, int triple_word);
void update_player_score (int player, int turn_score);
void show_key_cb (GtkWidget * w, gpointer data);
void change_key_mode (void);

/* ************************************* */

#endif

/* Gnerudite_h ***/
