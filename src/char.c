/* char.c
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "Gnerudite.h"

extern gint number_of_letters_left;
extern rack_button tile_button[13];
extern int letters[100];
extern int letters_left[27];
extern int tiles_in_rack;
extern int debug;
extern int bag_pos;
extern gchar bag[100];

gchar pick_letter_from_bag (void)
{
/* No longer used. This function has been replaced. */

  int r;
  TOMTRACE (("About to start pick_letter_from_bag\n"));

  TOMTRACE (("Number letters left is %d\n", number_of_letters_left));

  if (number_of_letters_left == 0)
    {
      g_print ("Out of letters!\n");
      gtk_exit (1);
    }

  r = rand () % 100;
  while (letters_left[letters[r]] == 0)
    r = rand () % 100;

  letters_left[letters[r]]--;
  number_of_letters_left--;
  return (char) (64 + letters[r]);
}

void
deal_seven_letters (void)
{
  int i = 0, j = 0;
  int k = (7 - tiles_in_rack);
  TOMTRACE (("Inside and about to start deal_seven_letters\n"));
  for (i = 0; i < k; i++)
    {
      for (j = 3; j < 10; j++)
	{
	  if (tile_button[j].letter == '\0')
	    {
	      rack_button_assign_char (&tile_button[j], letter_bag_pop ());
	      tiles_in_rack++;
	      break;
	    }
	}
    }
  TOMTRACE (("Finished deal_seven_letters\n"));
}

gchar
letter_bag_pop (void)
{
  bag_pos--;
  if (bag_pos < 0)
    {
      TOMTRACE (("The letter bag is empty!\n"));
      return '\0';
    }
  return bag[bag_pos];
}

void
letter_bag_push (gchar c)
{
  if (bag_pos >= 100)
    {
      printf (("Error, bag overflowed\n"));
      gtk_exit (13);
    }
  bag[bag_pos] = c;
  bag_pos++;
}

void
letter_bag_mix (void)
{
  /* TODO mix up the bag array */
  int i = 0, k = 0, l = 0;
  gchar c;

/* Do lots of swaps to randomise the Bag */
  for (i = 0; i < 500; i++)
    {
      k = rand () % bag_pos;
      l = rand () % bag_pos;
      c = bag[k];
      bag[k] = bag[l];
      bag[l] = c;
    }
}

void
letter_bag_create (void)
{
  /* TODO This is ugly, and uses an old routine to work */
  int r;
  int letters_remaining[27] =
    { 2, 9, 2, 2, 4, 12, 2, 3, 2, 9, 1, 1, 4, 2, 6, 8, 2,
    1, 6, 4, 6, 4, 2, 2, 1, 2, 1
  };

  number_of_letters_left = 100;

  while (number_of_letters_left > 0)
    {
      r = rand () % 100;
      while (letters_remaining[letters[r]] == 0)
	r = rand () % 100;

      letters_remaining[letters[r]]--;
      number_of_letters_left--;
      letter_bag_push ((char) (64 + letters[r]));
    }
}

void
cb_jumble_button (void)
{
  int i = 0, k = 0, l = 0;
  char temp_rack[7];
  char c;
  if (tiles_in_rack < 2)
    return;

  /* Tidy up players rack */
  tidy_rack ();

  /* Now stuff it into an array */
  for (i = 0; i < tiles_in_rack; i++)
    temp_rack[i] = tile_button[i + 3].letter;

/* Now do lots of swaps to randomise temp_rack */
  for (i = 0; i < 100; i++)
    {
      k = rand () % tiles_in_rack;
      l = rand () % tiles_in_rack;
      c = temp_rack[k];
      temp_rack[k] = temp_rack[l];
      temp_rack[l] = c;
    }

  /* Now stick the letters back */
  for (i = 3; i < tiles_in_rack + 3; i++)
    rack_button_assign_char (&tile_button[i], temp_rack[i - 3]);
}

void
tidy_rack (void)
{
  char temp_rack[13] =
    { '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
    '\0'
  };
  int i = 0, j = 0;

  for (i = 0; i < 13; i++)
    {
      temp_rack[i] = tile_button[i].letter;
      rack_button_assign_char (&tile_button[i], '\0');
    }

  for (i = 0, j = 3; i < 13; i++)
    {
      if (!temp_rack[i] == '\0')
	{
	  rack_button_assign_char (&tile_button[j], temp_rack[i]);
	  j++;
	}
    }
}
