/* init.c
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "Gnerudite.h"

extern int debug;
extern user_preferences options;
extern rack_button tile_button[13];
extern grid_button button[15][15];
extern grid_button *other_gridbutton;
extern rack_button *other_rackbutton;
extern int grid_selected_count;
extern int rack_selected_count;
extern int scrabble[15][15];
extern int first_go;
extern GdkFont *myfont, *myfont2;
extern GtkWidget *tile_rack, *main_window_vbox, *score_vbox;
extern GtkWidget *scrolled_window;
extern GtkWidget *score_label, *end_go_button, *score_label2;
extern GtkWidget *deal_button, *jumble_button;
extern GtkWidget *main_window_inner_vbox;
extern GtkWidget *table;
extern GtkWidget *closebutton;
extern GtkWidget *window, *main_window_hbox, *status;
extern gint number_of_letters_left;
extern GnomeClient *client;
extern GtkWidget *swap_tile_button;
extern GtkWidget *key_handlebox;


void
setup_preferences (void)
{
  TOMTRACE (("About to load user prefs\n"));

  options.propwindow = NULL;
  options.theme_entry = NULL;
  options.host_entry = NULL;
  options.font_entry = NULL;


  load_user_preferences ();
  myfont = gdk_font_load (options.tile_font);
  if (!myfont)
    myfont = gdk_font_load ("-*-helvetica-*-r-*-*-12-*-*-*-*-*-*-*");
  if (!myfont)
    myfont = gdk_font_load ("fixed");

  TOMTRACE (("User prefs loaded\n"));

  /*
   * This parses the gtkrc file I have included to control the look
   * of the toggle_buttons for the grid_tiles
   * This has the effect of making the board themable. I can save the
   * directory name as a user_preference, and then gtk_rc_parse the gtkrc in
   * whichever directory the user likes. The option _can_ exist to use the
   * current gtk theme, with no custmisations, too.
   */
  TOMTRACE (("About to parse the file %s\n",
	     g_concat_dir_and_file (options.tile_theme_dir, "gtkrc")));

  gtk_rc_parse (g_concat_dir_and_file (options.tile_theme_dir, "gtkrc"));

  TOMTRACE (("About to load the dictionary file %s\n",
	     options.dictionary_file));
  load_dictionary ();
}


void
init_create_board (void)
{
  GtkStyle *grid_button_style;
  int i = 0, j = 0;

  /* ------------------------------------------------------- */
  /* create a table of 15 by 15 squares. */
  table = gtk_table_new (15, 15, TRUE);
  /* set the spacing to 10 on x and 10 on y */
  gtk_table_set_row_spacings (GTK_TABLE (table), 0);
  gtk_table_set_col_spacings (GTK_TABLE (table), 0);
  /* pack the table into the scrolled window */
  gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW
					 (scrolled_window), table);
  gtk_widget_show (table);

  /* ------------------------------------------------------- */
  /* A 15 by 15 array of tile_buttons to simulate the board. */
  for (i = 0; i < 15; i++)
    for (j = 0; j < 15; j++)
      {
	(button[j][i]).toggle_button = gtk_toggle_button_new ();

	switch (button[j][i].score)
	  {
	  case SCORE_NONE:
	    {
	      gtk_widget_set_name (button[j][i].toggle_button, "blank");
	      break;
	    }
	  case SCORE_DOUBLE_LETTER:
	    {
	      gtk_widget_set_name (button[j][i].toggle_button, "2l");
	      break;
	    }
	  case SCORE_TRIPLE_LETTER:
	    {
	      gtk_widget_set_name (button[j][i].toggle_button, "3l");
	      break;
	    }
	  case SCORE_DOUBLE_WORD:
	    {
	      gtk_widget_set_name (button[j][i].toggle_button, "2w");
	      break;
	    }
	  case SCORE_TRIPLE_WORD:
	    {
	      gtk_widget_set_name (button[j][i].toggle_button, "3w");
	      break;
	    }
	  default:
	    {
	      TOMTRACE (("Error setting togglebutton class\n"));
	    }
	  }
	button[j][i].tile_table = gtk_table_new (2, 2, TRUE);
	button[j][i].label = gtk_label_new ("");
	button[j][i].score_label = gtk_label_new ("");
	button[j][i].letter = '\0';
	button[j][i].row = j;
	button[j][i].col = i;
	button[j][i].placed_this_go = FALSE;
	button[j][i].is_wildcard = FALSE;

	/* Here we assign the user_font */
	/* TODO We should theme this too! */
	grid_button_style = gtk_widget_get_style (button[j][i].label);
	grid_button_style->font = myfont;
	gtk_widget_set_style (button[j][i].label, grid_button_style);

	gtk_table_attach (GTK_TABLE (button[j][i].tile_table),
			  button[j][i].label,
			  0, 1,
			  0, 1,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK, 0, 0);

	gtk_table_attach (GTK_TABLE (button[j][i].tile_table),
			  button[j][i].score_label,
			  1, 2,
			  1, 2,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK, 0, 0);

	gtk_container_add (GTK_CONTAINER (button[j][i].toggle_button),
			   button[j][i].tile_table);
	gtk_container_set_border_width (GTK_CONTAINER
					((button[j][i]).toggle_button), 0);
	gtk_widget_show (button[j][i].label);
	gtk_widget_show (button[j][i].score_label);
	gtk_widget_show (button[j][i].tile_table);

	gtk_table_attach (GTK_TABLE (table),
			  (button[j][i]).toggle_button,
			  j, j + 1,
			  i, i + 1,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK, 0, 0);

	switch (options.tile_swap_mode)
	  {
	  case TILE_SWAP_PRESS:
	    {
	      gtk_signal_connect_object (GTK_OBJECT
					 ((button[j][i]).toggle_button),
					 "pressed",
					 (GtkSignalFunc)
					 grid_square_toggled,
					 ((gpointer) & button[j][i]));
	      break;
	    }
	  case TILE_SWAP_RELEASE:
	    {
	      gtk_signal_connect_object (GTK_OBJECT
					 ((button[j][i]).toggle_button),
					 "toggled",
					 (GtkSignalFunc)
					 grid_square_toggled,
					 ((gpointer) & button[j][i]));
	      break;
	    }
	  case TILE_SWAP_DRAG:
	    {
	      TOMTRACE (("Drag mode not yet implemented!\n"));
	      gtk_signal_connect_object (GTK_OBJECT
					 ((button[j][i]).toggle_button),
					 "toggled",
					 (GtkSignalFunc)
					 grid_square_toggled,
					 ((gpointer) & button[j][i]));
	      break;
	    }
	  default:
	    {
	      g_print (("Error, incorrect tile_swap setting\n"));
	      gtk_exit (3);
	      break;
	    }
	  }
	gtk_widget_show ((button[j][i]).toggle_button);
      }
}

void
init_create_rack (char *playername)
{
  int j = 0;
  GtkStyle *rack_button_style;
  GtkWidget *rack_dock;
  GtkWidget *rack_vbox;
  GdkFont *myscorefont;
  GtkWidget *rack_hbox;
  GtkWidget *rack_player_label;
  GtkWidget *score_label;

  TOMTRACE (("About to create the tile racks\n"));

  score_label = gtk_label_new (_ ("Score:"));
  score_label2 = gtk_label_new ("0");
  rack_dock = gtk_handle_box_new ();
  rack_vbox = gtk_vbox_new (FALSE, 2);
  rack_hbox = gtk_hbox_new (FALSE, 2);
  if (playername)
    rack_player_label = gtk_label_new (playername);
  else
    rack_player_label = gtk_label_new ("Player Name");
  tile_rack = gtk_hbox_new (TRUE, 0);
  myscorefont =
    gdk_font_load ("-misc-fixed-*-r-normal-*-*-80-*-*-c-*-iso8859-1");
  for (j = 0; j < 13; j++)
    {
      tile_button[j].tile_table = gtk_table_new (2, 2, TRUE);
      tile_button[j].toggle_button = gtk_toggle_button_new ();
      tile_button[j].score_label = gtk_label_new ("");
      gtk_widget_set_usize (GTK_WIDGET (tile_button[j].toggle_button), 30,
			    -1);
      gtk_widget_set_name (tile_button[j].toggle_button, "rack");
      gtk_box_pack_start (GTK_BOX (tile_rack),
			  tile_button[j].toggle_button, TRUE, TRUE, 1);
      tile_button[j].label = gtk_label_new ("");
      tile_button[j].letter = '\0';
      tile_button[j].col = j;
      tile_button[j].is_wildcard = FALSE;

      /* Set the user_font for the rack */
      rack_button_style = gtk_widget_get_style (tile_button[j].label);
      rack_button_style->font = myfont;
      gtk_widget_set_style (tile_button[j].label, rack_button_style);

      /* Set the font for the score label
         score_label_style = gtk_widget_get_style (tile_button[j].score_label);
         score_label_style->font = myscorefont;
         gtk_widget_set_style (tile_button[j].score_label, score_label_style);
       */

      gtk_table_attach (GTK_TABLE (tile_button[j].tile_table),
			tile_button[j].label,
			0, 1,
			0, 1,
			GTK_FILL | GTK_EXPAND | GTK_SHRINK,
			GTK_FILL | GTK_EXPAND | GTK_SHRINK, 0, 0);

      gtk_table_attach (GTK_TABLE (tile_button[j].tile_table),
			tile_button[j].score_label,
			1, 2,
			1, 2,
			GTK_FILL | GTK_EXPAND | GTK_SHRINK,
			GTK_FILL | GTK_EXPAND | GTK_SHRINK, 0, 0);

      gtk_container_add (GTK_CONTAINER
			 (tile_button[j].toggle_button),
			 tile_button[j].tile_table);
      gtk_widget_show (tile_button[j].label);
      gtk_widget_show (tile_button[j].score_label);
      gtk_widget_show (tile_button[j].tile_table);

      gtk_signal_connect_object (GTK_OBJECT
				 (tile_button[j].toggle_button),
				 "toggled",
				 (GtkSignalFunc) rack_tile_toggled,
				 (gpointer) & tile_button[j]);
      gtk_widget_show (tile_button[j].toggle_button);
    }

  gtk_widget_set_usize (GTK_WIDGET (tile_rack), -1, 30);

  gtk_box_pack_start (GTK_BOX (rack_hbox), rack_player_label, FALSE, FALSE,
		      0);
  gtk_box_pack_end (GTK_BOX (rack_hbox), score_label2, FALSE, FALSE, 2);
  gtk_box_pack_end (GTK_BOX (rack_hbox), score_label, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (rack_vbox), rack_hbox, TRUE, TRUE, 2);
  gtk_box_pack_start (GTK_BOX (main_window_vbox), rack_dock, FALSE, TRUE, 0);
  gtk_box_pack_end (GTK_BOX (rack_vbox), tile_rack, TRUE, TRUE, 0);
  gtk_container_add (GTK_CONTAINER (rack_dock), rack_vbox);

/*  GTK_HANDLE_BOX (rack_dock)->shrink_on_detach = FALSE;  */

  gtk_widget_show (score_label);
  gtk_widget_show (score_label2);
  gtk_widget_show (tile_rack);
  gtk_widget_show (rack_dock);
  gtk_widget_show (rack_hbox);
  gtk_widget_show (rack_vbox);
  gtk_widget_show (rack_player_label);
}

void
init_create_score_and_buttons (void)
{
  GtkWidget *end_go_button;
  GtkWidget *jumble_button;
  GtkWidget *skip_go_button;
  GtkWidget *button_handlebox;
  GtkWidget *button_vbox;
  GtkWidget *button_hbox;

  /* Key to special bonuses. */
  GtkWidget *key_hbox;
  GtkWidget *key_frame;
  GtkWidget *key_inner_vbox;
  GtkWidget *key_blank_hbox, *key_blank_button, *key_blank_label;
  GtkWidget *key_2l_hbox, *key_2l_button, *key_2l_label;
  GtkWidget *key_3l_hbox, *key_3l_button, *key_3l_label;
  GtkWidget *key_2w_hbox, *key_2w_button, *key_2w_label;
  GtkWidget *key_3w_hbox, *key_3w_button, *key_3w_label;

  main_window_inner_vbox = gtk_vbox_new (FALSE, 0);
  gnome_app_set_contents (GNOME_APP (window), main_window_inner_vbox);
  main_window_hbox = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (main_window_inner_vbox),
		      main_window_hbox, TRUE, TRUE, 0);
  gtk_widget_show (main_window_hbox);
  score_vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (score_vbox), 5);
  gtk_box_pack_end (GTK_BOX (main_window_hbox), score_vbox, FALSE, TRUE, 0);

  end_go_button = gtk_button_new_with_label (_ ("End Turn"));
  deal_button = gtk_button_new_with_label (_ ("Start Game"));
  jumble_button = gtk_button_new_with_label (_ ("Jumble Rack"));
  swap_tile_button = gtk_button_new_with_label (_ ("Swap some tiles"));
  skip_go_button = gtk_button_new_with_label (_ ("Skip this Turn"));

  key_inner_vbox = gtk_vbox_new (TRUE, 0);
  key_hbox = gtk_hbox_new (TRUE, 0);
  key_handlebox = gtk_handle_box_new ();
  key_frame = gtk_frame_new (_ ("Key"));
  button_handlebox = gtk_handle_box_new ();
  button_vbox = gtk_vbox_new (TRUE, 0);
  button_hbox = gtk_hbox_new (TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (button_handlebox), 4);

  gtk_container_set_border_width (GTK_CONTAINER (key_handlebox), 5);

  GTK_HANDLE_BOX (key_handlebox)->shrink_on_detach = FALSE;

  key_blank_hbox = gtk_hbox_new (FALSE, 2);
  key_2l_hbox = gtk_hbox_new (FALSE, 2);
  key_3l_hbox = gtk_hbox_new (FALSE, 2);
  key_2w_hbox = gtk_hbox_new (FALSE, 2);
  key_3w_hbox = gtk_hbox_new (FALSE, 2);

  key_blank_button = gtk_toggle_button_new ();
  gtk_widget_set_name (key_blank_button, "blank");
  key_2l_button = gtk_toggle_button_new ();
  gtk_widget_set_name (key_2l_button, "2l");
  key_3l_button = gtk_toggle_button_new ();
  gtk_widget_set_name (key_3l_button, "3l");
  key_2w_button = gtk_toggle_button_new ();
  gtk_widget_set_name (key_2w_button, "2w");
  key_3w_button = gtk_toggle_button_new ();
  gtk_widget_set_name (key_3w_button, "3w");

  gtk_widget_set_sensitive (key_blank_button, FALSE);
  gtk_widget_set_sensitive (key_2l_button, FALSE);
  gtk_widget_set_sensitive (key_3l_button, FALSE);
  gtk_widget_set_sensitive (key_2w_button, FALSE);
  gtk_widget_set_sensitive (key_3w_button, FALSE);

  gtk_widget_set_usize (key_blank_button, 20, 20);
  gtk_widget_set_usize (key_2l_button, 20, 20);
  gtk_widget_set_usize (key_3l_button, 20, 20);
  gtk_widget_set_usize (key_2w_button, 20, 20);
  gtk_widget_set_usize (key_3w_button, 20, 20);

  key_blank_label = gtk_label_new (_ ("Normal square"));
  key_2l_label = gtk_label_new (_ ("Double Letter"));
  key_3l_label = gtk_label_new (_ ("Triple Letter"));
  key_2w_label = gtk_label_new (_ ("Double Word"));
  key_3w_label = gtk_label_new (_ ("Triple Word"));

  /* Signals */
  gtk_signal_connect_object (GTK_OBJECT (end_go_button),
			     "clicked", (GtkSignalFunc) go_ended, NULL);
  gtk_signal_connect_object (GTK_OBJECT (deal_button),
			     "clicked", (GtkSignalFunc) cb_deal_button, NULL);
  gtk_signal_connect_object (GTK_OBJECT (jumble_button),
			     "clicked", (GtkSignalFunc) cb_jumble_button,
			     NULL);
  gtk_signal_connect_object (GTK_OBJECT (swap_tile_button),
			     "clicked", (GtkSignalFunc) cb_swap_tile_button,
			     NULL);
  gtk_signal_connect_object (GTK_OBJECT (skip_go_button),
			     "clicked", (GtkSignalFunc) cb_skip_go_pressed,
			     NULL);
  /* Packing */
  gtk_box_pack_start (GTK_BOX (score_vbox), key_hbox, FALSE, FALSE, 1);
  gtk_box_pack_start (GTK_BOX (key_hbox), key_handlebox, FALSE, FALSE, 1);
  gtk_container_add (GTK_CONTAINER (key_handlebox), key_frame);
  gtk_container_add (GTK_CONTAINER (key_frame), key_inner_vbox);

  gtk_box_pack_start (GTK_BOX (key_inner_vbox), key_blank_hbox, FALSE, FALSE,
		      1);
  gtk_box_pack_start (GTK_BOX (key_inner_vbox), key_2l_hbox, FALSE, FALSE, 1);
  gtk_box_pack_start (GTK_BOX (key_inner_vbox), key_3l_hbox, FALSE, FALSE, 1);
  gtk_box_pack_start (GTK_BOX (key_inner_vbox), key_2w_hbox, FALSE, FALSE, 1);
  gtk_box_pack_start (GTK_BOX (key_inner_vbox), key_3w_hbox, FALSE, FALSE, 1);

  gtk_box_pack_start (GTK_BOX (key_blank_hbox), key_blank_button, FALSE,
		      FALSE, 1);
  gtk_box_pack_start (GTK_BOX (key_2l_hbox), key_2l_button, FALSE, FALSE, 1);
  gtk_box_pack_start (GTK_BOX (key_3l_hbox), key_3l_button, FALSE, FALSE, 1);
  gtk_box_pack_start (GTK_BOX (key_2w_hbox), key_2w_button, FALSE, FALSE, 1);
  gtk_box_pack_start (GTK_BOX (key_3w_hbox), key_3w_button, FALSE, FALSE, 1);

  gtk_box_pack_start (GTK_BOX (key_blank_hbox), key_blank_label, FALSE, FALSE,
		      1);
  gtk_box_pack_start (GTK_BOX (key_2l_hbox), key_2l_label, FALSE, FALSE, 1);
  gtk_box_pack_start (GTK_BOX (key_3l_hbox), key_3l_label, FALSE, FALSE, 1);
  gtk_box_pack_start (GTK_BOX (key_2w_hbox), key_2w_label, FALSE, FALSE, 1);
  gtk_box_pack_start (GTK_BOX (key_3w_hbox), key_3w_label, FALSE, FALSE, 1);

  gtk_container_add (GTK_CONTAINER (button_handlebox), button_vbox);
  gtk_box_pack_end (GTK_BOX (button_hbox), button_handlebox, FALSE, FALSE, 2);
  gtk_box_pack_end (GTK_BOX (score_vbox), button_hbox, FALSE, FALSE, 2);
  gtk_box_pack_end (GTK_BOX (button_vbox), end_go_button, FALSE, FALSE, 2);
  gtk_box_pack_end (GTK_BOX (button_vbox), jumble_button, FALSE, FALSE, 2);
  gtk_box_pack_end (GTK_BOX (button_vbox), skip_go_button, FALSE, FALSE, 2);
  gtk_box_pack_end (GTK_BOX (button_vbox), swap_tile_button, FALSE, FALSE, 2);
  gtk_box_pack_end (GTK_BOX (button_vbox), deal_button, FALSE, FALSE, 2);
  /* Show widgets */
  gtk_widget_show (score_vbox);
  gtk_widget_show (key_hbox);
  gtk_widget_show (key_inner_vbox);

  if (options.show_key)
    gtk_widget_show (key_handlebox);

  gtk_widget_show (key_frame);
  gtk_widget_show (key_blank_hbox);
  gtk_widget_show (key_2l_hbox);
  gtk_widget_show (key_3l_hbox);
  gtk_widget_show (key_2w_hbox);
  gtk_widget_show (key_3w_hbox);
  gtk_widget_show (key_blank_label);
  gtk_widget_show (key_2l_label);
  gtk_widget_show (key_3l_label);
  gtk_widget_show (key_2w_label);
  gtk_widget_show (key_3w_label);
  gtk_widget_show (key_blank_button);
  gtk_widget_show (key_2l_button);
  gtk_widget_show (key_3l_button);
  gtk_widget_show (key_2w_button);
  gtk_widget_show (key_3w_button);

  gtk_widget_show (deal_button);
  gtk_widget_show (end_go_button);
  gtk_widget_show (skip_go_button);
  gtk_widget_show (jumble_button);
  gtk_widget_show (swap_tile_button);
  gtk_widget_show (button_handlebox);
  gtk_widget_show (button_vbox);
  gtk_widget_show (button_hbox);

  TOMTRACE (("Successfully setup score box and buttons\n"));
}

void
init_session_management (char *name)
{
  GnomeClient *client;

  /* Get the master client, that was hopefully connected to the
     session manager int the 'gnome_init' call.  All communication
     to the session manager will be done with this master client. */
  client = gnome_master_client ();

  /* Arrange to be told when something interesting happens.  */
  gtk_signal_connect (GTK_OBJECT (client), "save_yourself",
		      GTK_SIGNAL_FUNC (save_yourself), (gpointer) name);
  gtk_signal_connect (GTK_OBJECT (client), "die",
		      GTK_SIGNAL_FUNC (die), NULL);

  /*check if we are connected to a session manager */
  if (GNOME_CLIENT_CONNECTED (client))
    {
      /*we are connected, we will get the prefix under which
         we saved our session last time and load up our data */
      gnome_config_push_prefix (gnome_client_get_config_prefix (client));

      TOMTRACE (("We are connected to the session manager\n"));
      /* We will eventually save our data to this prefix when the
       * session terminates, and load it back here, to resume a game.
       */
      /*         some_value = gnome_config_get_int("Section/Key=0");  */

      gnome_config_pop_prefix ();
    }
  else
    {
      /*we are not connected to any session manager, here we
         will just initialize a new game like we would normally
         do without a session manager */
      TOMTRACE (("We are NOT connected to the session manager\n"));
    }
}
