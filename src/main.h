/* This enables verbose output for tracing errors */
int debug = TRUE;

/* This defines the board, or at least where the special scores are */
int scrabble[15][15] = {
  {4, 0, 0, 1, 0, 0, 0, 4, 0, 0, 0, 1, 0, 0, 4},
  {0, 2, 0, 0, 0, 3, 0, 0, 0, 3, 0, 0, 0, 2, 0},
  {0, 0, 2, 0, 0, 0, 1, 0, 1, 0, 0, 0, 2, 0, 0},
  {1, 0, 0, 2, 0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 1},
  {0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0},
  {0, 3, 0, 0, 0, 3, 0, 0, 0, 3, 0, 0, 0, 3, 0},
  {0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0},
  {4, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 1, 0, 0, 4},
  {0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0},
  {0, 3, 0, 0, 0, 3, 0, 0, 0, 3, 0, 0, 0, 3, 0},
  {0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0},
  {1, 0, 0, 2, 0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 1},
  {0, 0, 2, 0, 0, 0, 1, 0, 1, 0, 0, 0, 2, 0, 0},
  {0, 2, 0, 0, 0, 3, 0, 0, 0, 3, 0, 0, 0, 2, 0},
  {4, 0, 0, 1, 0, 0, 0, 4, 0, 0, 0, 1, 0, 0, 4},
};

/*
 *  This is the "letter bag" which stores the correct number
 *  of each letter available, by * alphabetical value (0 is a
 *  wildcard, 1 is A, 2 is B etc.
 */
int letters[100] = { 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2,
  3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6,
  6, 7, 7, 7, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10, 11, 12,
  12, 12, 12, 13, 13, 14, 14, 14, 14, 14, 14, 15, 15, 15,
  15, 15, 15, 15, 15, 16, 16, 17, 18, 18, 18, 18, 18, 18,
  19, 19, 19, 19, 20, 20, 20, 20, 20, 20, 21, 21, 21, 21,
  22, 22, 23, 23, 24, 25, 25, 26
};

/*
 *  How many of each letter is left? letters_left[0] is how many
 *  wildcards, letters_left[1] is how many A's etc.
 */
int letters_left[27] = { 2, 9, 2, 2, 4, 12, 2, 3, 2, 9, 1, 1, 4, 2, 6, 8, 2,
  1, 6, 4, 6, 4, 2, 2, 1, 2, 1
};

/* Scores array, from 0 (wildcard) to 26 (z) */
int scores[27] =
  { 0, 1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4,
  8, 4, 10
};

/* Quick cheap check , if lettersused == 100, we've run out */
int lettersused = 0;

/* Which player's go is it? Not yet used */
int who = 0;

/* This keeps the scores for each player (arbitrarily set to 8)
 * Eventually there will be options for changing the rules, and you could
 * technically specify unlimited letters, and 20 players over the net...
 * This arbitrary limit _will_ go soon.
 * Currently not used!
 */
int score[8] = { 0, 0 };

/* Is this the first move? If so, must use the center tile */
int first_go = TRUE;

GdkFont *myfont, *myfont2;
GtkWidget *tile_rack, *main_window_vbox, *score_vbox;
GtkWidget *scrolled_window;
GtkWidget *main_window_inner_vbox;
GtkWidget *score_label2;
GtkWidget *deal_button;
GtkWidget *table;
GtkWidget *closebutton;
GtkWidget *swap_dialog;
GtkWidget *swap_tile_button;
grid_button button[15][15];
grid_button *other_gridbutton;
rack_button *other_rackbutton;
rack_button tile_button[13];
int grid_selected_count = 0;
int rack_selected_count = 0;
int tiles_in_rack = 0;
GtkWidget *window, *main_window_hbox, *status;
gint number_of_letters_left = 100;
user_preferences options;
user_preferences old_options;
int swapping_tiles = FALSE;
int players_scores[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
GtkWidget *key_handlebox;


/* Word search stuff */
char dict[DICTSIZE][16];
int word_pos[15];
int numperms[7];
short int perms[7][5040][7];

/* Char push/pop stuff */
gchar bag[100];
int bag_pos = 0;
