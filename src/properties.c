/* properties.c
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/* This stuff is partially lifted from the sound-monitor applet, as it has
 * some of the nicest property-related code I've come across, and this has
 * saved me loads of time :) Credit to John Ellis.
 * Eventually, this will look _very_ different, and will probably all
 * change, but this has given me a running start...
 */

#include "Gnerudite.h"

extern int debug;
extern user_preferences options;
extern user_preferences old_options;
extern GtkWidget *key_handlebox;

void property_apply_cb (GtkWidget * widget, void *nodata, gpointer data);
gint property_destroy_cb (GtkWidget * widget, gpointer data);

void
theme_selected_cb (GtkWidget * clist, gint row, gint column,
		   GdkEventButton * bevent, gpointer data)
{
  user_preferences *pref = data;
  gchar *text = gtk_clist_get_row_data (GTK_CLIST (clist), row);
  if (text)
    gtk_entry_set_text (GTK_ENTRY (pref->theme_entry), text);
}

gint sort_theme_list_cb (void *a, void *b)
{
  return strcmp ((gchar *) a, (gchar *) b);
}

void
populate_theme_list (GtkWidget * clist)
{
  DIR *dp;
  struct dirent *dir;
  struct stat ent_sbuf;
  gint row;
  gchar *buf[] = { "x", };
  gchar *themepath;
  GList *theme_list = NULL;
  GList *list;

  /* add default theme */
/*
  buf[0] = _ ("None (Use your current Gtk Theme)");
  row = gtk_clist_append (GTK_CLIST (clist), buf);
  gtk_clist_set_row_data (GTK_CLIST (clist), row, "None");
*/

/*  themepath = gnome_unconditional_datadir_file ("Gnerudite");  */
  themepath = g_concat_dir_and_file (GNOMEDATADIR, "Gnerudite/themes");

  if ((dp = opendir (themepath)) == NULL)
    {
      /* dir not found */
      g_free (themepath);
      return;
    }

  while ((dir = readdir (dp)) != NULL)
    {
      /* skips removed files */
      if (dir->d_ino > 0)
	{
	  gchar *name;
	  gchar *path;

	  name = dir->d_name;
	  path = g_strconcat (themepath, "/", name, NULL);

	  if (stat (path, &ent_sbuf) >= 0 && S_ISDIR (ent_sbuf.st_mode))
	    {
	      theme_list = g_list_insert_sorted (theme_list,
						 g_strdup (name),
						 (GCompareFunc)
						 sort_theme_list_cb);
	    }
	  g_free (path);
	}
    }
  closedir (dp);

  list = theme_list;
  while (list)
    {
      gchar *themedata_file = g_strconcat (themepath, "/", list->data,
					   "/gtkrc", NULL);
      if (g_file_exists (themedata_file))
	{
	  gchar *tile_theme_dir =
	    g_strconcat (themepath, "/", list->data, NULL);
	  buf[0] = list->data;
	  row = gtk_clist_append (GTK_CLIST (clist), buf);
	  gtk_clist_set_row_data_full (GTK_CLIST (clist), row,
				       tile_theme_dir,
				       (GtkDestroyNotify) g_free);
	}
      g_free (themedata_file);
      g_free (list->data);
      list = list->next;
    }

  g_list_free (theme_list);

  g_free (themepath);
}

void
property_apply_cb (GtkWidget * widget, void *nodata, gpointer data)
{
  user_preferences *ad = &options;
  user_preferences *old = &old_options;
  gchar *buf;
  gint info_changed = FALSE;

  buf = gtk_entry_get_text (GTK_ENTRY (ad->theme_entry));

  if (buf)
    {
      if (strcmp (buf, old->tile_theme_dir))
	{
	  /* Different theme selected */
	  if (ad->tile_theme_dir)
	    g_free (ad->tile_theme_dir);
	  TOMTRACE (("Copying new theme into options\n"));
	  ad->tile_theme_dir = g_strdup (buf);
	  info_changed = TRUE;
	  change_theme ();
	}
      else
	{
	  TOMTRACE (("Same theme as before was left selected\n"));
	}
    }
  else
    {
      TOMTRACE (("Theme entry was left blank.\n"));
    }

  if (old->confirm_skip_go != ad->confirm_skip_go)
    {
      TOMTRACE (("Confirm skip go was changed\n"));
      info_changed = TRUE;
    }

  if (old->show_key != ad->show_key)
    {
      TOMTRACE (("Show_key was changed\n"));
      change_key_mode ();
      info_changed = TRUE;
    }

  if (old->tile_swap_mode != ad->tile_swap_mode)
    {
      TOMTRACE (("Tile Swap mode was changed!\n"));
      change_tile_swap_mode ();
      info_changed = TRUE;
    }

  buf = gtk_entry_get_text (GTK_ENTRY (ad->font_entry));
  if ((buf) && (strcmp (buf, ad->tile_font)))
    {
      TOMTRACE (("Font was changed\n"));
      if (ad->tile_font)
	{
	  g_free (ad->tile_font);
	  ad->tile_font = NULL;
	}
      ad->tile_font = g_strdup (buf);
      info_changed = TRUE;
      change_tile_font ();
    }

  if (info_changed)
    {
      TOMTRACE (("User preferences saved\n"));
      save_user_preferences ();
      /* Re-copy options for next comparison */
      TOMTRACE (("About to memcpy options again\n"));
      memcpy (&old_options, &options, sizeof (user_preferences));
    }
  else
    TOMTRACE (("User preferences not changed\n"));

}

gint property_destroy_cb (GtkWidget * widget, gpointer data)
{
  options.propwindow = NULL;
  return FALSE;
}

void
show_preferences_dialog (GtkWidget * widget, gpointer data)
{
  user_preferences *ad = &options;
  GtkWidget *frame;
  GtkWidget *frame2;
  GtkWidget *pref_vbox;
  GtkWidget *pref_vbox_2;
  GtkWidget *pref_hbox_3;
  GtkWidget *pref_hbox;
  GtkWidget *label;
  GtkWidget *button;
  GtkWidget *button2;
  GtkWidget *font_button;
  GtkWidget *radio_group;
  GtkWidget *radio_group_magic;
  GtkObject *adj;
  GtkWidget *hscale;
  GtkWidget *scrolled;
  GtkWidget *theme_clist;
  gchar *theme_title[] = { "Themes:", };

  if (ad->propwindow)
    {
      TOMTRACE (("propwindow is already open, I'm showing it..."));
      gdk_window_raise (ad->propwindow->window);
      return;
    }

  /* Grab old property settings for comparison */
  TOMTRACE (("About to memcpy options\n"));
  memcpy (&old_options, &options, sizeof (user_preferences));

  ad->propwindow = gnome_property_box_new ();
  gtk_window_set_title (GTK_WINDOW
			(&GNOME_PROPERTY_BOX (ad->propwindow)->dialog.window),
			_ ("Gnerudite Settings"));

  TOMTRACE (("Created new property window\n"));

  pref_vbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
  gtk_container_set_border_width (GTK_CONTAINER (pref_vbox), GNOME_PAD_SMALL);

  frame = gtk_frame_new (_ ("Swap tiles when Button is (TODO)"));
  gtk_box_pack_start (GTK_BOX (pref_vbox), frame, FALSE, FALSE, 0);
  gtk_widget_show (frame);

  pref_vbox_2 = gtk_hbox_new (TRUE, GNOME_PAD_SMALL);
  gtk_container_set_border_width (GTK_CONTAINER (pref_vbox_2),
				  GNOME_PAD_SMALL);
  gtk_container_add (GTK_CONTAINER (frame), pref_vbox_2);
  gtk_widget_show (pref_vbox_2);

  radio_group = gtk_radio_button_new_with_label (NULL, _ ("Released"));

  if (ad->tile_swap_mode == TILE_SWAP_RELEASE)
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (radio_group), 1);
  gtk_signal_connect (GTK_OBJECT (radio_group), "clicked",
		      (GtkSignalFunc) tile_swap_release_cb, NULL);

  gtk_box_pack_start (GTK_BOX (pref_vbox_2), radio_group, FALSE, FALSE, 0);
  gtk_widget_show (radio_group);

  button =
    gtk_radio_button_new_with_label (gtk_radio_button_group
				     (GTK_RADIO_BUTTON (radio_group)),
				     _ ("Pressed"));
  if (ad->tile_swap_mode == TILE_SWAP_PRESS)
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), 1);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) tile_swap_press_cb, NULL);

  gtk_box_pack_start (GTK_BOX (pref_vbox_2), button, FALSE, FALSE, 0);
  gtk_widget_show (button);

  button =
    gtk_radio_button_new_with_label (gtk_radio_button_group
				     (GTK_RADIO_BUTTON (radio_group)),
				     _ ("Dragged"));
  if (ad->tile_swap_mode == TILE_SWAP_DRAG)
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), 1);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) tile_swap_drag_cb, NULL);

  gtk_box_pack_start (GTK_BOX (pref_vbox_2), button, FALSE, FALSE, 0);
  gtk_widget_show (button);


  button = gtk_check_button_new_with_label (_ ("Confirm skip go?"));
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button),
				ad->confirm_skip_go);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) skip_go_cb, NULL);
  gtk_box_pack_start (GTK_BOX (pref_vbox), button, FALSE, FALSE, 0);
  gtk_widget_show (button);

  button = gtk_check_button_new_with_label (_ ("Show Key?"));
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), ad->show_key);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) show_key_cb, NULL);
  gtk_box_pack_start (GTK_BOX (pref_vbox), button, FALSE, FALSE, 0);
  gtk_widget_show (button);

  frame = gtk_frame_new (_ ("This does nothing yet :)"));
  gtk_box_pack_start (GTK_BOX (pref_vbox), frame, FALSE, FALSE, 0);
  gtk_widget_show (frame);

  pref_vbox_2 = gtk_hbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (pref_vbox_2),
				  GNOME_PAD_SMALL);
  gtk_container_add (GTK_CONTAINER (frame), pref_vbox_2);
  gtk_widget_show (pref_vbox_2);

  adj = gtk_adjustment_new (5, 1.0, 10.0, 1.0, 1.0, 0.0);
  hscale = gtk_hscale_new (GTK_ADJUSTMENT (adj));
  gtk_range_set_update_policy (GTK_RANGE (hscale), GTK_UPDATE_DELAYED);
  gtk_scale_set_digits (GTK_SCALE (hscale), 0);
/*
	gtk_signal_connect( GTK_OBJECT(adj),"value_changed",GTK_SIGNAL_FUNC(falloff_speed_cb), ad);
*/
  gtk_box_pack_start (GTK_BOX (pref_vbox_2), hscale, TRUE, TRUE, 0);
  gtk_widget_show (hscale);

  frame = gtk_frame_new (_ ("This does nothing yet :)"));
  gtk_box_pack_start (GTK_BOX (pref_vbox), frame, FALSE, FALSE, 0);
  gtk_widget_show (frame);

  pref_vbox_2 = gtk_vbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (pref_vbox_2),
				  GNOME_PAD_SMALL);
  gtk_container_add (GTK_CONTAINER (frame), pref_vbox_2);
  gtk_widget_show (pref_vbox_2);

  adj = gtk_adjustment_new (4, 1.0, 10.0, 1.0, 1.0, 0.0);
  hscale = gtk_hscale_new (GTK_ADJUSTMENT (adj));
  gtk_range_set_update_policy (GTK_RANGE (hscale), GTK_UPDATE_DELAYED);
  gtk_scale_set_digits (GTK_SCALE (hscale), 0);
/*	
	gtk_signal_connect( GTK_OBJECT(adj),"value_changed",GTK_SIGNAL_FUNC(scope_scale_cb), ad);
*/
  gtk_box_pack_start (GTK_BOX (pref_vbox_2), hscale, FALSE, FALSE, 0);
  gtk_widget_show (hscale);

  /* --------- */

  frame = gtk_frame_new (_ ("This does nothing yet :)"));
  gtk_box_pack_start (GTK_BOX (pref_vbox), frame, FALSE, FALSE, 0);
  gtk_widget_show (frame);

  pref_vbox_2 = gtk_vbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (pref_vbox_2),
				  GNOME_PAD_SMALL);
  gtk_container_add (GTK_CONTAINER (frame), pref_vbox_2);
  gtk_widget_show (pref_vbox_2);

  adj = gtk_adjustment_new (10, 5.0, 40.0, 1.0, 1.0, 0.0);
  hscale = gtk_hscale_new (GTK_ADJUSTMENT (adj));
  gtk_range_set_update_policy (GTK_RANGE (hscale), GTK_UPDATE_DELAYED);
  gtk_scale_set_digits (GTK_SCALE (hscale), 0);
/*
	gtk_signal_connect( GTK_OBJECT(adj),"value_changed",GTK_SIGNAL_FUNC(refresh_fps_cb), ad);
*/
  gtk_box_pack_start (GTK_BOX (pref_vbox_2), hscale, FALSE, FALSE, 0);
  gtk_widget_show (hscale);

  label = gtk_label_new (_ ("Behaviour Settings"));
  gtk_widget_show (pref_vbox);
  gnome_property_box_append_page (GNOME_PROPERTY_BOX (ad->propwindow),
				  pref_vbox, label);

  /* theme tab */

  frame = gtk_frame_new (NULL);
  gtk_container_set_border_width (GTK_CONTAINER (frame), GNOME_PAD_SMALL);

  pref_vbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
  gtk_container_set_border_width (GTK_CONTAINER (pref_vbox), GNOME_PAD_SMALL);
  gtk_container_add (GTK_CONTAINER (frame), pref_vbox);
  gtk_widget_show (pref_vbox);

  pref_hbox = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start (GTK_BOX (pref_vbox), pref_hbox, FALSE, FALSE, 0);
  gtk_widget_show (pref_hbox);

  label = gtk_label_new (_ ("Theme file (directory):"));
  gtk_box_pack_start (GTK_BOX (pref_hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);

  ad->theme_entry = gtk_entry_new ();

  if (ad->tile_theme_dir)
    gtk_entry_set_text (GTK_ENTRY (ad->theme_entry), ad->tile_theme_dir);
  gtk_signal_connect_object (GTK_OBJECT (ad->theme_entry), "changed",
			     GTK_SIGNAL_FUNC (gnome_property_box_changed),
			     GTK_OBJECT (ad->propwindow));
  gtk_box_pack_start (GTK_BOX (pref_hbox), ad->theme_entry, TRUE, TRUE, 0);
  gtk_widget_show (ad->theme_entry);

  scrolled = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
  gtk_box_pack_start (GTK_BOX (pref_vbox), scrolled, TRUE, TRUE, 0);
  gtk_widget_show (scrolled);

  /* theme list */
  theme_clist = gtk_clist_new_with_titles (1, theme_title);
  gtk_clist_column_titles_passive (GTK_CLIST (theme_clist));
  gtk_signal_connect (GTK_OBJECT (theme_clist), "select_row",
		      (GtkSignalFunc) theme_selected_cb, ad);
  gtk_container_add (GTK_CONTAINER (scrolled), theme_clist);
  gtk_widget_show (theme_clist);

  populate_theme_list (theme_clist);

  label = gtk_label_new (_ ("Board Theme"));
  gtk_widget_show (frame);
  gnome_property_box_append_page (GNOME_PROPERTY_BOX (ad->propwindow),
				  frame, label);

  /* Look and feel :) */

  frame = gtk_frame_new (NULL);
  gtk_container_set_border_width (GTK_CONTAINER (frame), GNOME_PAD_SMALL);

  pref_vbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
  gtk_container_set_border_width (GTK_CONTAINER (pref_vbox), GNOME_PAD_SMALL);
  gtk_container_add (GTK_CONTAINER (frame), pref_vbox);
  gtk_widget_show (pref_vbox);

  pref_hbox = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start (GTK_BOX (pref_vbox), pref_hbox, FALSE, FALSE, 0);
  gtk_widget_show (pref_hbox);

  label = gtk_label_new (_ ("Font to use for Tiles."));
  gtk_box_pack_start (GTK_BOX (pref_hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);

  ad->font_entry = gtk_entry_new ();
  if (ad->font_entry)
    gtk_entry_set_text (GTK_ENTRY (ad->font_entry), ad->tile_font);

  gtk_signal_connect_object (GTK_OBJECT (ad->font_entry), "changed",
			     GTK_SIGNAL_FUNC (gnome_property_box_changed),
			     GTK_OBJECT (ad->propwindow));

  gtk_box_pack_start (GTK_BOX (pref_hbox), ad->font_entry, TRUE, TRUE, 0);
  gtk_widget_show (ad->font_entry);

/* Font selector button */
  pref_hbox = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start (GTK_BOX (pref_vbox), pref_hbox, FALSE, FALSE, 0);
  gtk_widget_show (pref_hbox);
  font_button = gtk_button_new_with_label (_ ("Pick Font..."));
  gtk_signal_connect_object (GTK_OBJECT (font_button), "clicked",
			     GTK_SIGNAL_FUNC (font_button_pressed), NULL);
  gtk_box_pack_end (GTK_BOX (pref_hbox), font_button, FALSE, FALSE, 0);
  gtk_widget_show (font_button);
  gtk_widget_show (pref_hbox);


  frame2 = gtk_frame_new (_ ("Extra"));
  gtk_box_pack_end (GTK_BOX (pref_vbox), frame2, FALSE, FALSE, 0);
  gtk_widget_show (frame2);

  pref_hbox_3 = gtk_hbox_new (TRUE, GNOME_PAD_SMALL);
  gtk_container_set_border_width (GTK_CONTAINER (pref_hbox_3),
				  GNOME_PAD_SMALL);
  gtk_container_add (GTK_CONTAINER (frame2), pref_hbox_3);
  gtk_widget_show (pref_hbox_3);

  radio_group_magic = gtk_radio_button_new_with_label (NULL, _ ("Magic"));

  if (ad->magic == FALSE)
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (radio_group_magic), 1);
  gtk_signal_connect (GTK_OBJECT (radio_group_magic), "clicked",
		      (GtkSignalFunc) magic_cb, NULL);

  gtk_box_pack_start (GTK_BOX (pref_hbox_3), radio_group_magic, FALSE, FALSE,
		      0);
  gtk_widget_show (radio_group_magic);

  button2 =
    gtk_radio_button_new_with_label (gtk_radio_button_group
				     (GTK_RADIO_BUTTON (radio_group_magic)),
				     _ ("More Magic"));
  if (ad->magic == TRUE)
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button2), 1);
  gtk_signal_connect (GTK_OBJECT (button2), "clicked",
		      (GtkSignalFunc) more_magic_cb, NULL);

  gtk_box_pack_start (GTK_BOX (pref_hbox_3), button2, FALSE, FALSE, 0);
  gtk_widget_show (button2);


  label = gtk_label_new (_ ("Look and Feel"));
  gtk_widget_show (frame);
  gnome_property_box_append_page (GNOME_PROPERTY_BOX (ad->propwindow),
				  frame, label);

  gtk_signal_connect (GTK_OBJECT (ad->propwindow), "apply",
		      GTK_SIGNAL_FUNC (property_apply_cb), ad);
  gtk_signal_connect (GTK_OBJECT (ad->propwindow), "destroy",
		      GTK_SIGNAL_FUNC (property_destroy_cb), ad);

  gtk_widget_show_all (ad->propwindow);
}


void
skip_go_cb (GtkWidget * w, gpointer data)
{
  options.confirm_skip_go = GTK_TOGGLE_BUTTON (w)->active;
  gnome_property_box_changed (GNOME_PROPERTY_BOX (options.propwindow));
}

void
show_key_cb (GtkWidget * w, gpointer data)
{
  options.show_key = GTK_TOGGLE_BUTTON (w)->active;
  gnome_property_box_changed (GNOME_PROPERTY_BOX (options.propwindow));
}

void
tile_swap_release_cb (GtkWidget * w, gpointer data)
{
  if (GTK_TOGGLE_BUTTON (w)->active)
    {
      options.tile_swap_mode = TILE_SWAP_RELEASE;
      gnome_property_box_changed (GNOME_PROPERTY_BOX (options.propwindow));
    }
}

void
tile_swap_press_cb (GtkWidget * w, gpointer data)
{
  if (GTK_TOGGLE_BUTTON (w)->active)
    {
      options.tile_swap_mode = TILE_SWAP_PRESS;
      gnome_property_box_changed (GNOME_PROPERTY_BOX (options.propwindow));
    }
}

void
tile_swap_drag_cb (GtkWidget * w, gpointer data)
{
  if (GTK_TOGGLE_BUTTON (w)->active)
    {
      options.tile_swap_mode = TILE_SWAP_DRAG;
      gnome_property_box_changed (GNOME_PROPERTY_BOX (options.propwindow));
    }
}

void
magic_cb (GtkWidget * w, gpointer data)
{

  if (GTK_TOGGLE_BUTTON (w)->active)
    {
      TOMTRACE (("Uh-oh, only Magic is selected\n"));
      options.magic = GTK_TOGGLE_BUTTON (w)->active;
      gnome_property_box_changed (GNOME_PROPERTY_BOX (options.propwindow));
    }
}

void
more_magic_cb (GtkWidget * w, gpointer data)
{
  if (GTK_TOGGLE_BUTTON (w)->active)
    {
      TOMTRACE (("That's better, More Magic is selected\n"));
      options.magic = GTK_TOGGLE_BUTTON (w)->active;
      gnome_property_box_changed (GNOME_PROPERTY_BOX (options.propwindow));
    }
}

int
load_user_preferences (void)
{
  char *buffer = NULL;
  char *buffer2 = NULL;
  char *buffer3 = NULL;
  char *buffer4 = NULL;
  char *buffer5 = NULL;
  gboolean def = FALSE;
  gboolean def2 = FALSE;

/* Save ourselves some typing :) */
  gnome_config_push_prefix ("/Gnerudite/settings/");

  /* --------------------------------------------------------- */
  /* Here we will load a user-defined font which will be later
   * defined by a properties window.
   * The equals-sign provides a default font.
   * I'm not sure what the defult should be! Fixed?
   */
  TOMTRACE (("About to get the font\n"));
  buffer =
    gnome_config_get_string
    ("tile_font=-misc-fixed-bold-r-normal-*-*-140-*-*-c-*-iso8859-1");
  options.tile_font = g_strdup (buffer);

  if (buffer == NULL)
    buffer = g_strdup ("-misc-fixed-bold-r-normal-*-*-140-*-*-c-*-iso8859-1");

  TOMTRACE (("Font is %s\n", buffer));

  g_free (buffer);

  TOMTRACE (("About to get the theme file\n"));

  /* Here we load the name of the directory from which to load the theme
   * if a theme is to be loaded. The directories will be installed to
   * $datadir/Gnerudite so there could be directories called default,
   * colourful, pastel, classic etc. Currently there is just pixmaps!
   */
  buffer2 =
    gnome_config_get_string_with_default ("tile_theme_dir=Default", &def);

  if (def)
    {
      buffer3 =
	g_strdup (g_concat_dir_and_file
		  (GNOMEDATADIR, "Gnerudite/themes/Classic"));
      options.tile_theme_dir = g_strdup (buffer3);
    }
  else
    {
      options.tile_theme_dir = g_strdup (buffer2);
    }

  TOMTRACE (("Theme dir is %s\n", options.tile_theme_dir));

  if (def)
    g_free (buffer3);
  else
    g_free (buffer2);


  /* File to use as our dictionary */
  buffer4 =
    gnome_config_get_string_with_default ("dictionary_file=Default", &def2);

  if (def2)
    {
      buffer5 =
	g_strdup (g_concat_dir_and_file
		  (GNOMEDATADIR, "Gnerudite/dictionary.gz"));
      options.dictionary_file = g_strdup (buffer5);
    }
  else
    {
      options.dictionary_file = g_strdup (buffer4);
    }

  TOMTRACE (("Dictionary file is %s\n", options.dictionary_file));

  if (def2)
    g_free (buffer5);
  else
    g_free (buffer4);


  TOMTRACE (("About to get confirm_skip_go\n"));

  /* Here we load a boolean which states whether we should offer confirmation
   * when a go is skipped.
   */
  options.confirm_skip_go = gnome_config_get_int ("confirm_skip_go=1");

  TOMTRACE (("About to get tile_swap_mode\n"));

  /* Here we load an int which decides the tile swap mode */
  options.tile_swap_mode = gnome_config_get_int ("tile_swap_mode=0");

  options.show_key = gnome_config_get_int ("show_key=1");

  options.magic = gnome_config_get_int ("magic=1");

  gnome_config_pop_prefix ();


  return TRUE;
}

int
save_user_preferences (void)
{

  /* Save ourselves some typeing :) */
  gnome_config_push_prefix ("/Gnerudite/settings/");

  /* This is fairly self-explanatory I think */
  gnome_config_set_string ("tile_font", options.tile_font);
  gnome_config_set_string ("tile_theme_dir", options.tile_theme_dir);
  gnome_config_set_int ("confirm_skip_go", options.confirm_skip_go);
  gnome_config_set_int ("tile_swap_mode", options.tile_swap_mode);
  gnome_config_set_int ("magic", options.magic);
  gnome_config_set_int ("show_key", options.show_key);
  gnome_config_sync ();

  gnome_config_pop_prefix ();

  return TRUE;
}

void
font_button_pressed (GtkWidget * w, gpointer data)
{
  GtkWidget *fs;

  TOMTRACE (("Font Button Pressed\n"));

  fs = gtk_font_selection_dialog_new ("Select Tile Font");
  gtk_font_selection_dialog_set_font_name (GTK_FONT_SELECTION_DIALOG (fs),
					   gtk_entry_get_text (GTK_ENTRY
							       (options.
								font_entry)));

  gtk_signal_connect (GTK_OBJECT (GTK_FONT_SELECTION_DIALOG (fs)->ok_button),
		      "clicked", GTK_SIGNAL_FUNC (font_sel_ok), fs);
  gtk_signal_connect (GTK_OBJECT
		      (GTK_FONT_SELECTION_DIALOG (fs)->cancel_button),
		      "clicked", GTK_SIGNAL_FUNC (font_sel_cancel), fs);

  gtk_widget_show (fs);
}

void
font_sel_ok (GtkWidget * w, GtkWidget * fsel)
{
  gtk_entry_set_text (GTK_ENTRY (options.font_entry),
		      gtk_font_selection_dialog_get_font_name
		      (GTK_FONT_SELECTION_DIALOG (fsel)));
  gtk_widget_destroy (fsel);
}

void
font_sel_cancel (GtkWidget * w, GtkWidget * fsel)
{
  gtk_widget_destroy (fsel);
}

void
change_key_mode (void)
{
  if (options.show_key)
    gtk_widget_show (key_handlebox);
  else
    gtk_widget_hide (key_handlebox);
}
