/* theme.c
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "Gnerudite.h"

extern user_preferences options;
extern grid_button button[15][15];
extern int debug;
extern GtkWidget *window;
extern rack_button tile_button[13];

void
change_theme (void)
{
/*  int i, j;
  GtkStyle *mystyle;
  char * buffer; */
  if (options.tile_theme_dir)
    {
      if (!strcmp (options.tile_theme_dir, "None"))
	{
	  g_free (options.tile_theme_dir);
	  options.tile_theme_dir =
	    g_strdup (g_concat_dir_and_file
		      (GNOMEDATADIR, "Gnerudite/themes/None"));
	}
      /* Change to this theme */
      TOMTRACE (("Changing to theme %s\n", options.tile_theme_dir));
      TOMTRACE (("About to re-assign styles\n"));
      /* try as last resort? */
      gtk_rc_parse (g_concat_dir_and_file (options.tile_theme_dir, "gtkrc"));
      gtk_widget_reset_rc_styles (window);
      gtk_rc_reparse_all ();

/*
      for (i = 0; i < 15; i++)
         for (j = 0; j < 15; j++)
         {
         buffer=gtk_widget_get_name(button[i][j].toggle_button);
	 gtk_widget_set_name(button[i][j].toggle_button,buffer);
         }
  */
    }
  else
    {
      TOMTRACE (("Error, NULL theme dir\n"));
      gtk_exit (0);
    }
}

void
change_tile_swap_mode (void)
{
  int i, j;
  TOMTRACE (("TODO! Changing tile_swap_mode\n"));

  for (i = 0; i < 15; i++)
    for (j = 0; j < 15; j++)
      {
/*	gtk_signal_disconnect_by_func (GTK_OBJECT
				       (button[i][j].toggle_button),
				       grid_square_toggled,
				       (gpointer) & button[i][j]);
 */
	gtk_signal_handlers_destroy (GTK_OBJECT (button[i][j].toggle_button));

	switch (options.tile_swap_mode)
	  {
	  case TILE_SWAP_PRESS:
	    {
	      gtk_signal_connect_object (GTK_OBJECT
					 ((button[i][j]).toggle_button),
					 "clicked",
					 (GtkSignalFunc)
					 grid_square_toggled,
					 ((gpointer) & button[i][j]));
	      break;
	    }
	  case TILE_SWAP_RELEASE:
	    {
	      gtk_signal_connect_object (GTK_OBJECT
					 ((button[i][j]).toggle_button),
					 "toggled",
					 (GtkSignalFunc)
					 grid_square_toggled,
					 ((gpointer) & button[i][j]));
	      break;
	    }
	  case TILE_SWAP_DRAG:
	    {
	      TOMTRACE (("Drag mode not yet implemented!\n"));
	      gtk_signal_connect_object (GTK_OBJECT
					 ((button[i][j]).toggle_button),
					 "toggled",
					 (GtkSignalFunc)
					 grid_square_toggled,
					 ((gpointer) & button[i][j]));
	      break;
	    }
	  default:
	    {
	      TOMTRACE (("Error, incorrect tile_swap setting"));
	      gtk_exit (3);
	      break;
	    }
	  }
      }
}

void
change_tile_font (void)
{
  GtkStyle *style, *style2;
  GdkFont *newfont;
  int i, j;

  TOMTRACE (("About to change the tile font\n"));

/* TODO Hiding and showing the label is not a clean way of updating the
 * font if a letter is already showing. I must find another way... */

  newfont = gdk_font_load (options.tile_font);
  for (i = 0; i < 15; i++)
    for (j = 0; j < 15; j++)
      {
	gtk_widget_hide (button[j][i].label);
	style = gtk_widget_get_style (button[j][i].label);
	style->font = newfont;
	gtk_widget_set_style (button[j][i].label, style);
	gtk_widget_show (button[j][i].label);
      }

  for (i = 0; i < 13; i++)
    {
      gtk_widget_hide (tile_button[i].label);
      style2 = gtk_widget_get_style (tile_button[i].label);
      style2->font = newfont;
      gtk_widget_set_style (tile_button[i].label, style2);
      gtk_widget_ensure_style (tile_button[i].label);
      gtk_widget_show (tile_button[i].label);
    }
}
