/* wordsearch.c
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "Gnerudite.h"

extern user_preferences options;
extern int debug;
extern char dict[DICTSIZE][16];
extern int word_pos[15];
extern int numperms[7];
extern short int perms[7][5040][7];

void
load_dictionary ()
{
  FILE *fp;
  int count;
  int nextlen = 3;
  char sys[255];
  char unname[255] = "";

  fp = fopen (options.dictionary_file, "r");
  if (fp == NULL)
    {
      TOMTRACE (("Couldn't open dictionary %s\n", options.dictionary_file));
      exit (1);
    }

  g_snprintf (unname, sizeof (unname), "/tmp/.gnerudite_dict%d", getpid ());
  g_snprintf (sys, sizeof (sys), "gzip -cd %s > %s", options.dictionary_file,
	      unname);
  system (sys);

  fp = fopen (unname, "r");
  if (fp == NULL)
    {
      TOMTRACE
	(
	 ("Error during unzip of dictionary, if you don't have gunzip, use the non-zipped dictionary\n"));
      exit (1);
    }

  TOMTRACE (("Loading dictionary...\n"));
  count = 0;
  word_pos[0] = count;
  while (!feof (fp))
    {
      fscanf (fp, "%s\n", dict[count]);
      if (strlen (dict[count]) == nextlen)
	{
	  word_pos[nextlen - 2] = count;
	  /* word length borders = word length - 2 */
	  nextlen++;
	}
      count++;
      if (count >= DICTSIZE)
	{
	  TOMTRACE (("dictionary exceeds expected maximum size.\n"));
	  exit (1);
	}
    }
  word_pos[nextlen - 2] = count - 1;
  fclose (fp);

  if (unname[0] != '\0')
    unlink (unname);
}

int
wordsearch (char *word)
{
  int start, end;
  int found = 0;
  int compare, wordlength;
  int lastmid = -1;
  int middle = -2;

  wordlength = strlen (word);
  if (wordlength < 2)
    return found;
  switch (wordlength)
    {
    case 2:
      {
	start = word_pos[0];
	end = word_pos[1];
	break;
      }
    case 3:
      {
	start = word_pos[1];
	end = word_pos[2];
	break;
      }
    case 4:
      {
	start = word_pos[2];
	end = word_pos[3];
	break;
      }
    case 5:
      {
	start = word_pos[3];
	end = word_pos[4];
	break;
      }
    case 6:
      {
	start = word_pos[4];
	end = word_pos[5];
	break;
      }
    case 7:
      {
	start = word_pos[5];
	end = word_pos[6];
	break;
      }
    case 8:
      {
	start = word_pos[6];
	end = word_pos[7];
	break;
      }
    case 9:
      {
	start = word_pos[7];
	end = word_pos[8];
	break;
      }
    case 10:
      {
	start = word_pos[8];
	end = word_pos[9];
	break;
      }
    case 11:
      {
	start = word_pos[9];
	end = word_pos[10];
	break;
      }
    case 12:
      {
	start = word_pos[10];
	end = word_pos[11];
	break;
      }
    case 13:
      {
	start = word_pos[11];
	end = word_pos[12];
	break;
      }
    case 14:
      {
	start = word_pos[12];
	end = word_pos[13];
	break;
      }
    default:
      {
	start = word_pos[13];
	end = word_pos[14] + 1;
	break;
      }
    }

  while ((lastmid != middle) && (!found))
    {
      lastmid = middle;
      middle = ((end - start) / 2) + start;
      compare = strcmp (word, dict[middle]);
      if (!compare)
	found = 1;
      else if (compare > 0)
	{
	  start = middle;
	}
      else
	end = middle;
    }
  return found;
}
